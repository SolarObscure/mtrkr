(function () {
    "use strict";

    var module = angular.module("mtrkr");

    module.component("spending-impact", {
        templateUrl: "/spending-impact/spending-impact.component.html",
        bindings: {
            value: "<"
        },
        transclude: true,
        controllerAs: "spendingImpactController",
        controller: function () {
            var spendingImpactController = this;

            spendingImpactController.$onInit = function () {
                spendingImpactController.spendings = new Array(spendingImpactController.value);
            };

            spendingImpactController.$onChanges = function () {
                spendingImpactController.spendings = new Array(spendingImpactController.value);
            }
        }
    })
}());
