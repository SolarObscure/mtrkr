(function () {
    "use strict";

    var module = angular.module("mtrkr");

    function fetchSpendings($http) {
        return $http
            .get("/mtrkr/spendings.json")
            .then(function (response) {
                return response.data;
            });
    }

    function controller($http) {
        var spendingController = this;

        spendingController.spendings = [];
        spendingController.$onInit = function () {
            fetchSpendings($http)
                .then(function (spendings) {
                    spendingController.spendings = spendings;
                });
        };

        spendingController.addSpending = function (spending) {
            console.log(spending);
            spendingController.spendings.push(spending);
        };

        spendingController.removeSpending = function (spending) {
            var index = spendingController.spendings.indexOf(spending);
            spendingController.spendings.splice(index, 1);
        };

        spendingController.increaseImpact = function (spending) {
            if (spending.impact < 5) {
                spending.impact++;
            }
        };

        spendingController.decreaseImpact = function (spending) {
            if (spending.impact > 1) {
                spending.impact--;
            }
        };
    }

    module.component("spendingsList", {
        templateUrl: "spendings-list.component.html",
        controllerAs: "spendingController",
        controller: ["$http", controller]
    });

}());
